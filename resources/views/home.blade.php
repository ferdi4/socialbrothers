<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Socialbrothers</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>

    <div class="header-bg">
        <div class="bg-overlay d-flex justify-content-center align-items-center">
            <img src="https://socialbrothers.nl/wp-content/themes/social_brothers/assets/SBlogo.svg" alt="logo">
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12 col-form">
                <div class="bg-white">
                    <form method="POST" action="{{ route('store') }}" class="form-blog">
                        @csrf
                        <div class="form-group row d-flex flex-column">
                            <label for="email" class="col-md-4 col-form-label">{{ __('Berichtnaam') }}</label>

                            <div class="col-md-12">
                                <input type="text" class="form-control" name="title" value="" placeholder="Geen titel" required autofocus>
                            </div>
                        </div>

                        <div class="form-group row d-flex flex-column">
                            <label for="password" class="col-md-4 col-form-label">{{ __('Categorie') }}</label>

                            <div class="col-md-12">

                                <select name="category">
                                    @foreach($categories as $key => $category)
                                        @if($key === 0)
                                            <option value="" disabled selected>Geen categorie</option>
                                        @endif
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="form-group row d-flex flex-column">
                            <label for="password-confirm" class="col-md-4 col-form-label">{{ __('Bericht') }}</label>

                            <div class="col-md-12">
                                <textarea ty
                                          pe="text" class="form-control" name="description" required></textarea>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-orange">
                                    {{ __('Bericht aanmaken') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="row">
                    @foreach($blogs as $blog)
                        <div class="col-6 bg-white">
                            <div class="blog-thumbnail">
                                <div class="blog-header d-flex align-items-end justify-content-between" style="background-image:url({{$blog->category->image}})">
                                    <div>
                                        {{$blog->created_at->format('m/d/Y')}}
                                    </div>
                                    <div>
                                        {{$blog->category->title}}
                                    </div>
                                </div>
                                <h2>{{ $blog->title }}</h2>
                                <p>{{ $blog->description }}</p>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-orange">
                            {{ __('Meer laden') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/app.js') }}" ></script>
</body>
</html>
