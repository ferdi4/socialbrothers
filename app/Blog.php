<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['title', 'category_id', 'description'];

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }
}
