<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        $categories = Category::all();
        $blogs = Blog::all();

        return view('home')
            ->with('categories', $categories)
            ->with('blogs', $blogs);
    }

    public function store(Request $request)
    {
        Blog::create([
            'title' => $request->title,
            'category_id' => $request->category,
            'description' => $request->description,
        ]);

        return redirect()->back();
    }

    public function show()
    {
        $blogs = Blog::all();
        return $blogs;
    }

}
