<?php

use Illuminate\Database\Seeder;

class BlogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $blogs = [
            [
                'category_id' => '1',
                'title' => 'Sneeuwpret in de bergen',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '2',
                'title' => 'Bootje varen in water',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '3',
                'title' => 'Soms wordt de lucht paars',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '4',
                'title' => 'Varen met een boot',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '1',
                'title' => 'Sneeuwpop bouwen',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '2',
                'title' => 'Zwemmen in koud water',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '3',
                'title' => 'Wordt de lucht ook geel?',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '4',
                'title' => 'Zou hier ook vis zitten',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '2',
                'title' => 'Groote haaien kleine vissen',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
            [
                'category_id' => '1',
                'title' => 'Fietsen door de bergen',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus eget metus blandit, pharetra nisi eu, aliquet leo. Mauris lacinia commodo risus, id lobortis massa ultrices nec.',
            ],
        ];

        foreach($blogs as $blog){
            App\Blog::create($blog);
        }
    }
}
