<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'title'   => 'Mooi landschap',
                'image' => 'images/1.jpg',
            ],
            [
                'title'   => 'Nog een mooi landschap',
                'image' => 'images/2.jpg',
            ],
            [
                'title'   => 'Weer een mooi landschap',
                'image' => 'images/3.jpg',
            ],
            [
                'title'   => 'Nog steeds een mooi landschap',
                'image' => 'images/4.jpg',
            ],
        ];

        foreach($categories as $category){
            App\Category::create($category);
        }
    }
}
